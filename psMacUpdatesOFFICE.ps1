﻿#http://jongurgul.com/blog/get-stringhash-get-filehash/ 
Function Get-StringHash([String] $String,$HashName = "SHA256") { 
    $StringBuilder = New-Object System.Text.StringBuilder 
    [System.Security.Cryptography.HashAlgorithm]::Create($HashName).ComputeHash([System.Text.Encoding]::UTF8.GetBytes($String))|%{ 
        [Void]$StringBuilder.Append($_.ToString("x2")) 
    } 
    $StringBuilder.ToString() 
}

function DownloadUPdate ([Parameter(Mandatory=$true)]$Payload, [Parameter(Mandatory=$true)]$location){
    #Test-WritePath
    Write-Output "Starting $location - $collateral\$payload"
    
    if(Test-Path -Path "$collateral\$payload.hash"){
        $local_hash = Get-Content -Path "$collateral\$payload.hash"

    }else {
        $local_hash = ""
    }
    
    if(Test-Path -Path "$collateral\$payload"){
        $local_length = $(Get-Item -Path "$collateral\$payload").Length
    } else {
        $local_length = 0
    }

    $source_head = Invoke-WebRequest -UseBasicParsing -Uri $location -Method Head -ErrorAction Stop
    $source_hash = Get-StringHash -String $("$($source_head.Headers.'ETag')$($source_head.Headers.'Content-Length')$($source_head.Headers.'Last-Modified')")
    
    if(($source_hash -eq $local_hash) -and ($source_head.Headers.'Content-Length' -eq $local_length)){
        Write-Output "Skipping Download on $Payload as local cache is correct. Local_Length:$local_length Content-Length:$($source_head.Headers.'Content-Length') "
        return $true
    } else{
        Write-Output "Local Payload is missing or needs to be updated. Local_Length:$local_length Content-Length:$($source_head.Headers.'Content-Length') "
        #TEST BASELINE
        $collateral = "$collarteralFolder"
        Start-BitsTransfer -Source $location -Destination $("$collateral\$payload")
        $source_hash |Out-File -Force "$collateral\$payload.hash"

    }
}

Start-Transcript -Path "C:\Scripts\MAUCacheAdmin\Logs\MAUCache-$(Get-Date -Format 'yyMMddhhmmss').txt"

$channel = "Production"
$IISRoot="C:\inetpub" 
$IisFolder="MAUCache"
$TempShare="C:\Temp"

$PublishFolderName = $IisFolder
$publishBasePath = $IISRoot
$tempfolderLocation = $TempShare

#Test iis shared folder exists if not make it
if (!(test-path "$publishBasePath\$PublishFolderName")){
    New-Item -ItemType Directory -Path "$publishBasePath" -Name "$PublishFolderName"
}

$PublishFolder = "$publishBasePath\$PublishFolderName"
$tempFolder = $PublishFolder

$collarteralFolder = $PublishFolder

#Create Temp Structure
if(-not (Test-Path -path "$publishBasePath/$PublishFolderName")){
    New-Item -ItemType Directory -Path $publishBasePath -Name "$PublishFolderName"
}
if(-not (Test-Path -path "$PublishFolder/Collateral")){
    New-Item -ItemType Directory -Path "$PublishFolder" -Name "Collateral"
}

$SourcePKG = @()
if(Test-Path -Path $PublishFolder){
    $PublishedPKGs = Get-ChildItem -Path $PublishFolder -Filter '*.pkg'
    $PublishedPKGs = $PublishedPKGs.Name
}else{
    $PublishedPKGs = @()
}

switch ($channel)
{
  "Production"{$webUrlDownload = "https://officecdn.microsoft.com/pr/C1297A47-86C4-4C1F-97FA-950631F94777/OfficeMac/"}
  "External"{$webUrlDownload = "https://officecdn.microsoft.com/pr/1ac37578-5a24-40fb-892e-b89d85b6dfaa/OfficeMac/"}
  "InsiderFast"{$webUrlDownload = "https://officecdn.microsoft.com/pr/4B2D7701-0A4F-49C8-B4CB-0C2D4043F51F/OfficeMac/"}
}
Invoke-WebRequest -Uri "$webUrlDownload/builds.txt" -OutFile "$collarteralFolder\builds.txt"

$MAU_Apps = @(
    "0409MSAU03",
    "0409MSWD15",
    "0409XCEL15",
    "0409PPT315",
    "0409OPIM15",
    "0409ONMC15",
    "0409MSOF14",
    #"0409SUIT15", #Does Not Work
    "0409UCCP14",
    "0409MSFB16"
    )

foreach ($MAU_App in $MAU_Apps){
    $payload =""
    $locationstring = ""
    $UpdateVersions = ""
    Write-Output "$down"
    
    Invoke-WebRequest -Uri "$webUrlDownload$MAU_App.xml" -OutFile "$collarteralFolder\$MAU_App.xml"
    Invoke-WebRequest -Uri "$webUrlDownload$MAU_App.xml" -OutFile "$collarteralFolder\$MAU_App.xml"


    #get xml and find updateversion
    $log = "$collarteralFolder\$MAU_App.xml"
    $collateral = $collarteralFolder
    $patt = "<key>Update Version"
    $indx = Select-String $patt $log | ForEach-Object {$_.LineNumber}
    if ($indx.count -ge 2){
        $UpdateVersions= @((Get-Content $log)[$indx])
        $UpdateVersions=$UpdateVersions  -replace "</String>", ""
        $UpdateVersions=$UpdateVersions  -replace "<String>", ""
        $UpdateVersions=$UpdateVersions.trim()
        $pathtoput = "$($updateversions[0])"
    } elseif ($indx.count -eq 1){
        $UpdateVersions= @((Get-Content $log)[$indx])
        $UpdateVersions=$UpdateVersions  -replace "</String>", ""
        $UpdateVersions=$UpdateVersions  -replace "<String>", ""
        $UpdateVersions=$UpdateVersions.trim()
        $pathtoput="$($UpdateVersions)"
    } else {
        $pathtoput="Legacy"
    }

    #TEST COLLATERAL PATH EXISTS
    if (-not (Test-Path "$collateral\$pathtoput")){
        new-item -ItemType Directory -Path $collateral -Name $pathtoput -Verbose
    }

    Write-Output "$collateral\$pathtoput\$MAU_App.xml"    
    Copy-Item -Path "$collarteralFolder\$MAU_App.xml" -Destination "$collateral\$pathtoput\$MAU_App.xml" -Verbose
    Copy-Item -Path "$collarteralFolder\$MAU_App.cat" -Destination "$collateral\$pathtoput\$MAU_App.cat" -Verbose


    #PAYLOAD NAME
    $log = "$collarteralFolder\$MAU_App.xml"
    $patt = "<KEY>Payload"
    $indxp = Select-String $patt $log | ForEach-Object {$_.LineNumber}
    Write-Output "$indx $($MAU_App)"
    $payload=@((Get-Content $log)[$indxp])
    $payload=$payload -replace "</String>", ""
    $payload=$payload -replace "<String>", ""
    $payload=$payload.trim()

    #DOWNLOAD FILE
    $patt = "<KEY>Location"
    $indx = Select-String $patt $log | ForEach-Object {$_.LineNumber}
    $locationstring= @((Get-Content $log)[$indx])
    $locationstring=$locationstring  -replace "</String>", ""
    $locationstring=$locationstring  -replace "<String>", ""
    $locationstring=$locationstring.trim()

    if ($indxp.count -le 1){
        Write-Output "One Detected $payload $locationstring"
        
        $SourcePKG += $payload

        DownloadUPdate -Payload $payload -location $locationstring
    } else {
        for ($x = 0; $x -le ($($indxp.count)-1); $x += 1) {
            Write-Output "One Detected $x"
            $pay = $($payload[$x])
            $loc = $($locationstring[$x])
            $SourcePKG += $pay
            DownloadUPdate -Payload $pay -location $loc
        }
    }
}

if(-not $error) {  
    foreach ($PublishedPKG in $PublishedPKGs){
        if($SourcePKG -notcontains $PublishedPKG){
            Write-Output "REMOVE File $PublishedPKG"
            Remove-Item -Path "$PublishFolder\$PublishedPKG"
            Remove-Item -Path "$PublishFolder\$PublishedPKG.hash"
        } else {
            Write-Output "KEEP File $PublishedPKG"
        }
    }
} else {
    write-output "Errors in process skipping cleanup."
}