MAUCacheAdmin
==============
This utility is used to download and automaticly maintain a local repository of MAC Office 2016 Updates.

## Install
Install into folder C:\Scripts\MAUCacheAdmin

## Requirements
* 10-20gb of storage
* IIS(with mime type .pkg = application/octet-stream)
* Powershell 

## Usage
Powershell -File psMacUpdatesOFFICE.ps1

## Scheduled Task
You can use the scheduled task XML provided.
